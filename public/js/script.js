let button = document.getElementById('button');
let gameboard = document.getElementById('game');
let count = document.getElementById('count');

function start(){
  gameboard.innerHTML ="";
  for (var i = 0 ; i<10; i++) {
    numberTop = Math.round(Math.random() * (65-10)+1);
    numberLeft = Math.round(Math.random()* (95-10)+1);
    const target = document.createElement('div');
    target.className = "target";
    target.style.top = numberTop+"%";
    target.style.left = numberLeft+"%";
    gameboard.appendChild(target);
  }
  inGame()
}

function inGame(){
  let hit = 0;
  let targets = document.querySelectorAll('.target');
  let countNumber =0;
  targets.forEach(target=>{
    target.addEventListener('click',e=>{
      if (Math.round(countNumber)<10) {
          hit+=1;
          target.style.top = Math.round(Math.random() * (65-10)+1)+"%";
          target.style.left = Math.round(Math.random() * (90-10)+1)+"%";
      }
    })
  })
  let timer = setInterval(function(){
    countNumber +=0.1;
    count.innerHTML = "Time :"+Math.round(countNumber.toString()*10)/10;
    if ((Math.round(countNumber*10)/10)==10) {
      clearInterval(timer);endGame(hit)}}
    ,100);

}

function endGame(score){
  let printScore = document.getElementById('printScore');
  printScore.innerHTML ="Well done, your score is : "+score;

  let xml = new XMLHttpRequest();
  xml.open('POST','createscore', true);
  xml.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
  $requete = 'score='+score;
  xml.send($requete);
}
