<?php

namespace App\Entity;

use App\Repository\GamesRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GamesRepository::class)]
class Games
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date_games = null;

    #[ORM\ManyToOne(inversedBy: 'games')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Users $player_games = null;

    #[ORM\Column]
    private ?int $score_games = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateGames(): ?\DateTimeInterface
    {
        return $this->date_games;
    }

    public function setDateGames(\DateTimeInterface $date_games): self
    {
        $this->date_games = $date_games;

        return $this;
    }

    public function getPlayerGames(): ?Users
    {
        return $this->player_games;
    }

    public function setPlayerGames(?Users $player_games): self
    {
        $this->player_games = $player_games;

        return $this;
    }

    public function getScoreGames(): ?int
    {
        return $this->score_games;
    }

    public function setScoreGames(int $score_games): self
    {
        $this->score_games = $score_games;

        return $this;
    }
}
