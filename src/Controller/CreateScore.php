<?php
namespace App\Controller;

use App\Entity\Games;
use App\Repository\GamesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class CreateScore extends AbstractController{

    #[Route('createscore', name: 'app_createscore_index', methods: ['POST'])]
    public function index(Request $request,GamesRepository $gamesRepository){
            date_default_timezone_set("Europe/Paris");
            $score = $request->request->get('score');
            $user =  $this->getUser();
            $game = new Games();
            $game->setScoreGames($score)->setDateGames(new \DateTime())->setPlayerGames($user);
            $gamesRepository->save($game, true);

        return new Response('success');
    }
}
